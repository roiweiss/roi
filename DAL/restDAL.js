const axios = require('axios');


const getUsers = function()
{
    return axios.get("http://jsonplaceholder.typicode.com/users");
}

const getUser = function(id)
{
    return axios.get("http://jsonplaceholder.typicode.com/users/" + id);
}

module.exports = {getUsers, getUser}
