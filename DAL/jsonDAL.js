const jFile = require('jsonfile')
    


const getUsers = function()
{
    return new Promise((resolve,reject) =>
    {
        jFile.readFile(__dirname + "/users.json", (err, data) =>
        {
            if(err)
            {
                reject(err);
            }
            else
            {
                resolve(data)
            }
        })
    })
    
}

module.exports = {getUsers}