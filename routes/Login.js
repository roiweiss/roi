var express = require('express');
var router = express.Router();

const bl = require('../models/loginBL');

/* GET users listing. */
router.get('/', async function(req, res, next) {
  res.render('Login', { msg : ''})
});

router.post('/', async function(req, res, next) {

  let isExist = await bl.isUserExist(req.body.username , req.body.pwd);
  if(isExist)
  {
    req.session["authenticated"] = true;
    res.redirect("/users");
  }
  else
  {
    res.render("Login", {msg : "user name or password are wrong !"})
  }
  
});

module.exports = router;
