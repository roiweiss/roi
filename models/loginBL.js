const { json } = require('body-parser');
const jsonDAL = require('../DAL/jsonDAL');


const isUserExist = async function(username, pwd)
{   
    let usersData = await jsonDAL.getUsers();
    let usersLogindata = usersData.users;

    let obj = usersLogindata.find(x => x.username == username && x.pwd == pwd);

    if(obj)
    {
        return true;
    }
    else
    {
        return false;
    }

}

module.exports = {isUserExist}