const database = require('../configs/database');
exports.getAllPersons = function()
{
    return new Promise(async(resolve, reject) =>
    {
        data = await database.getUsers();
            resolve(data);
        
    });
}

exports.insert = function(s)
{
    return new Promise(async(resolve, reject) =>
    {
        data = await database.insertData(s);
            resolve(data);
        
    });
}
