const restDAL = require('../DAL/restDAL');
const jsonDAL = require('../DAL/jsonDAL');


const getUsers = async function()
{   
    //Get data from axios
    let resp = await restDAL.getUsers();
    let usersFromREST = resp.data;

    let usersData = usersFromREST.map(x => 
        {
            return { id : x.id , name : x.name}
        })

    return usersData;

}

const getUser = async function(id)
{   
    //Get data from axios
    let resp = await restDAL.getUser(id);
    
    let userData = resp.data;

    return userData;

}

module.exports = {getUsers, getUser}